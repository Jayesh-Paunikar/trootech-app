<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Models\User;
use App\Http\Resources\UserResource;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;


class UserController extends Controller
{


    public function register(RegisterRequest $request){

        $user = User::whereEmail($request->email)->first();

        if (!$user) {
            throw new ConflictHttpException('The email has already been taken');
        }
        $data = $request->validated();
        $user = User::create($data);


        $tokens = $this->requestAccessToken($request->email, $request->password);

        return (new UserResource($user->refresh()))->additional(['authentication' => $tokens]);


    }

    public function login(LoginRequest $request){

        $user = User::where('email',$request->email)->first();

        if (is_null($user)) {
            throw new UnauthorizedHttpException('', 'Incorrect email or password');
        }



        $tokens = $this->requestAccessToken($request->email, $request->password);

        return (new UserResource($user->refresh()))->additional(['authentication' => $tokens]);
    }


    public function requestAccessToken(string $email, string $password)
    {

        $uri = url('/oauth/token');

        $http = new \GuzzleHttp\Client;

        try {

            $response = $http->post($uri, [
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id' => env('PASSPORT_CLIENT_ID'),
                    'client_secret' => env('PASSPORT_CLIENT_SECRET'),
                    'username' => $email,
                    'password' => $password,
                    'scope' => '*',
                ],
            ]);

            return json_decode((string) $response->getBody(), true);
        } catch (GuzzleException $exception) {

            if (400 === $exception->getCode()) {
                throw new UnauthorizedHttpException('', 'Incorrect email or username or password');
            }
        }

    }



}
