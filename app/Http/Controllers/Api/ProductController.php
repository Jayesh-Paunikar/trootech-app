<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Http\Resources\ProductResource;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductCategory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    //
    public function categoryProduct($category_id){

        $product_id=ProductCategory::where('category_id',$category_id)->get();
        $product_id=$product_id->pluck('product_id')->toArray();
        $prodcuts=Product::with('ProductCategory','user_data')->where('user_id',Auth::user()->id)->whereIn('id',$product_id)->get();
        $prodcuts->map(function ($product) {
            $categoriesData=$product->ProductCategory->pluck('category_id')->toArray();
            $product['categories']=Category::whereIn('id',$categoriesData)->get();
            return $product;
        });
        return ProductResource::collection($prodcuts);

    }

    public function index(){

        $product_id=ProductCategory::get();
        $product_id=$product_id->pluck('product_id')->toArray();
        $prodcuts=Product::with('ProductCategory','user_data')->where('user_id',Auth::user()->id)->whereIn('id',$product_id)->get();
        $prodcuts->map(function ($product) {
            $categoriesData=$product->ProductCategory->pluck('category_id')->toArray();
            $product['categories']=Category::whereIn('id',$categoriesData)->get();
            return $product;
        });
        return ProductResource::collection($prodcuts);

    }

    public function store(ProductRequest $request){

        $data = $request->validated();
        $categories=explode(',',$data['category_id']);
        $query =new Product;
        $query->user_id=Auth::user()->id;
        $query->name=$data['name'];
        $query->price=$data['price'];
        $query->created_at=Carbon::now();
        $query->updated_at=Carbon::now();
        $query->save();
        $product_id=$query->id;
        foreach($categories as $cat){

            $prodcut_cat=new ProductCategory();
            $prodcut_cat->product_id=$product_id;
            $prodcut_cat->category_id=$cat;
            $prodcut_cat->created_at=Carbon::now();
            $prodcut_cat->updated_at=Carbon::now();
            $prodcut_cat->save();
        }
        $prodcuts=Product::with('ProductCategory','user_data')->whereId($product_id)->get();
        $prodcuts->map(function ($product) {
            $categoriesData=$product->ProductCategory->pluck('category_id')->toArray();
            $product['categories']=Category::whereIn('id',$categoriesData)->get();
            return $product;
        });
        return ProductResource::collection($prodcuts);
    }

    public function update($id, ProductRequest $request){

        $data = $request->validated();
        $categories=explode(',',$data['category_id']);
        $query =Product::whereId($id)->first();
        $query->user_id=1;//Auth::user()->id;
        $query->name=$data['name'];
        $query->price=$data['price'];
        $query->created_at=Carbon::now();
        $query->updated_at=Carbon::now();
        $query->save();
        $product_id=$query->id;
        ProductCategory::where('product_id',$product_id)->delete();
        foreach($categories as $cat){

            $prodcut_cat=new ProductCategory();
            $prodcut_cat->product_id=$product_id;
            $prodcut_cat->category_id=$cat;
            $prodcut_cat->created_at=Carbon::now();
            $prodcut_cat->updated_at=Carbon::now();
            $prodcut_cat->save();
        }
        $prodcuts=Product::with('ProductCategory','user_data')->whereId($product_id)->get();
        $prodcuts->map(function ($product) {
            $categoriesData=$product->ProductCategory->pluck('category_id')->toArray();
            $product['categories']=Category::whereIn('id',$categoriesData)->get();
            return $product;
        });
        return ProductResource::collection($prodcuts);
    }

    public function delete($id){

        Product::whereId($id)->delete();
        ProductCategory::where('product_id',$id)->delete();
        return [ 'message' => 'category deleted successfully' ];
    }
}
