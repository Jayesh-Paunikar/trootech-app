<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Http\Resources\CategoryResource;
use App\Http\Requests\CategoryRequest;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;

class CategoryController extends Controller
{
    public function index(){

        $categories = Category::whereNull('category_id')
        ->with('subcategories')
        ->paginate();
        return CategoryResource::collection($categories);

    }

    public function store(CategoryRequest $request){

        $data = $request->validated();
        $category = Category::create($data);
        return new CategoryResource($category);
    }

    public function update($id, CategoryRequest $request){

        $category = Category::find($id);
        $category->name = $request->name;
        $category->save();

        return new CategoryResource($category);
    }

    public function delete($id){

        $data = Category::where('category_id',$id)->get();
        if($data->count() > 0){
            throw new ConflictHttpException('This Category will not be deleted becaouse it has sub categories.');
        }

        Category::whereId($id)->delete();
        return [ 'message' => 'category deleted successfully' ];
    }



}
