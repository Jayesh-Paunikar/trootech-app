<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;


    public function ProductCategory()
    {
        return $this->hasMany(ProductCategory::class,'product_id');
    }
    public function user_data()
    {
        return $this->hasMany(User::class,'id','user_id');
    }
}
