<?php
/**
* @OpenAPI(
* @OA\Server(url=L5_SWAGGER_CONST_HOST, description="localhost")
* )
* @OA\Info(
* version="1.0.0",
* title="GTHR",
* description="TrooTech Apis",
* @OA\License(
* name="Apache 2.0",
* url="http://www.apache.org/licenses/LICENSE-2.0.html"
* )
* )
*
*  @OA\SecurityScheme(type="http", securityScheme="BearerAuth", scheme="bearer", bearerFormat="JWT"),
*/

/**
 * @OA\Post(
 *     path="/register",
 *     summary="Sign up process",
 *     tags={"Auth"},
 *     security={},
 *     @OA\RequestBody(description="", required=true,
 *         @OA\MediaType(mediaType="multipart/form-data",
 *             @OA\Schema(
 *                 @OA\Property(property="email", type="string", format="email", example="jayesh@gmail.com"),
 *                 @OA\Property(property="firstname", type="string", minimum="4", example="Jayesh"),
 *                 @OA\Property(property="lastname", type="string", minimum="4", example="Paunikar"),
 *                 @OA\Property(property="password", type="string", minimum="8", example="password"),
 *                 @OA\Property(property="password_confirmation", type="string", minimum="8", example="password"),
 *                 required={"email", "firstname","lastname", "password", "password_confirmation"}
 *             ),
 *         ),
 *     ),
 *     @OA\Response(response="200", description="Sign up successfully",
 *         @OA\MediaType(mediaType="application/json")
 *     ),
 *     @OA\Response(response="401", description="Unauthenticated",
 *         @OA\MediaType(mediaType="application/json")
 *     ),
 *     @OA\Response(response="422", description="Validation error",
 *         @OA\MediaType(mediaType="application/json")
 *     ),
 *  )

 * @OA\Post(
 *     path="/login",
 *     summary="Login Process",
 *     tags={"Auth"},
 *     security={},
 *     @OA\RequestBody(description="", required=true,
 *         @OA\MediaType(mediaType="application/json",
 *             @OA\Schema(
 *                 @OA\Property(property="email", type="string", format="email", example="jayesh@gmail.com"),
 *                 @OA\Property(property="password", type="string", minimum="8", example="password"),
 *                 required={"email","password"}
 *             ),
 *         ),
 *     ),
 *     @OA\Response(response="200", description="Login successfully",
 *         @OA\MediaType(mediaType="application/json")
 *     ),
 *     @OA\Response(response="401", description="Unauthenticated",
 *         @OA\MediaType(mediaType="application/json")
 *     ),
 *     @OA\Response(response="422", description="Validation error",
 *         @OA\MediaType(mediaType="application/json")
 *     ),
 *  )
 *
 */

/**
 * @OA\Get(
 *  path="/category/list",
 *  tags={"Category"}, security={{ "BearerAuth"={} }},
 *  summary="Get Category list ",
 *  @OA\Response(response="200", description="Category retreived successfully",
 *      @OA\MediaType(mediaType="application/json")
 *  ),
 * )
 */


/**
 * @OA\Post(
 *     path="/category/create",
 *     summary="Create Category",
 *     tags={"Category"}, security={{ "BearerAuth"={} }},
 *     @OA\RequestBody(description="", required=true,
 *         @OA\MediaType(mediaType="application/json",
 *             @OA\Schema(
 *                 @OA\Property(property="name", type="string", minimum="4", example="Demo Category"),
 *                 @OA\Property(property="category_id", type="string", minimum="4", example="1"),
 *                 required={"name"}
 *             ),
 *         ),
 *     ),
 *     @OA\Response(response="200", description="Category has been created",
 *         @OA\MediaType(mediaType="application/json")
 *     ),
 *     @OA\Response(response="401", description="Unauthenticated",
 *         @OA\MediaType(mediaType="application/json")
 *     ),
 *     @OA\Response(response="422", description="Validation error",
 *         @OA\MediaType(mediaType="application/json")
 *     ),
 *  )
 */

/**
 *  @OA\Put(path="/category/update/{id}", summary="Update Category", description="",
 *     tags={"Category"}, security={{ "BearerAuth"={} }},
 *     @OA\Parameter(in="path", name="id", required=true, example=1),
 *     @OA\RequestBody(description="", required=true,
 *         @OA\MediaType(mediaType="application/json",
 *            @OA\Schema(
 *                 @OA\Property(property="name", type="string", minimum="4", example="demo category"),
 *                 required={"name"}
 *             ),
 *         ),
 *     ),
 *     @OA\Response(response="200", description="Category info updated successfully",
 *         @OA\MediaType(mediaType="application/json")
 *     ),
 *     @OA\Response(response="401", description="Unauthenticated",
 *         @OA\MediaType(mediaType="application/json")
 *     ),
 *     @OA\Response(response="422", description="Validation error",
 *         @OA\MediaType(mediaType="application/json")
 *     ),
 *  )
 */

/**
 *  @OA\Delete(path="/category/delete/{id}", description="", summary="Delete Category",
 *     tags={"Category"}, security={{ "BearerAuth"={} }},
 *     @OA\Parameter(in="path", name="id", required=true, example=1),
 *     @OA\Response(response="200", description="Category deleted successfully",
 *         @OA\MediaType(mediaType="application/json")
 *     ),
 *     @OA\Response(response="401", description="Unauthenticated",
 *         @OA\MediaType(mediaType="application/json")
 *     ),
 *  )
 */




/**
 * @OA\Get(
 *  path="/product/{category_id}/list",
 *  tags={"Product"}, security={{ "BearerAuth"={} }},
 *     @OA\Parameter(in="path", name="category_id", required=true, example=1),
 *  summary="Get product list ",
 *  @OA\Response(response="200", description="product retreived successfully",
 *      @OA\MediaType(mediaType="application/json")
 *  ),
 * )
 */

/**
 * @OA\Get(
 *  path="/product/list",
 *  tags={"Product"}, security={{ "BearerAuth"={} }},
 *  summary="Get product list ",
 *  @OA\Response(response="200", description="product retreived successfully",
 *      @OA\MediaType(mediaType="application/json")
 *  ),
 * )
 */

/**
 * @OA\Post(
 *     path="/product/create",
 *     summary="Create product",
 *     tags={"Product"}, security={{ "BearerAuth"={} }},
 *     @OA\RequestBody(description="", required=true,
 *         @OA\MediaType(mediaType="application/json",
 *             @OA\Schema(
 *                 @OA\Property(property="name", type="string", minimum="4", example="Demo Product"),
 *                 @OA\Property(property="price", type="string", example="200"),
 *                 @OA\Property(property="category_id", type="string", example="1,2"),
 *                 required={"name","category_id","price"}
 *             ),
 *         ),
 *     ),
 *     @OA\Response(response="200", description="product has been created",
 *         @OA\MediaType(mediaType="application/json")
 *     ),
 *     @OA\Response(response="401", description="Unauthenticated",
 *         @OA\MediaType(mediaType="application/json")
 *     ),
 *     @OA\Response(response="422", description="Validation error",
 *         @OA\MediaType(mediaType="application/json")
 *     ),
 *  )
 */

/**
 *  @OA\Put(path="/product/update/{id}", summary="Update product information", description="",
 *     tags={"Product"}, security={{ "BearerAuth"={} }},
 *     @OA\Parameter(in="path", name="id", required=true, example=1),
 *     @OA\RequestBody(description="", required=true,
 *         @OA\MediaType(mediaType="application/json",
 *            @OA\Schema(
 *                 @OA\Property(property="name", type="string", minimum="4", example="Demo Product"),
 *                 @OA\Property(property="price", type="string", example="200"),
 *                 @OA\Property(property="category_id", type="string", example="1"),
 *                 required={"name","category_id","price"}
 *             ),
 *         ),
 *     ),
 *     @OA\Response(response="200", description="product info updated successfully",
 *         @OA\MediaType(mediaType="application/json")
 *     ),
 *     @OA\Response(response="401", description="Unauthenticated",
 *         @OA\MediaType(mediaType="application/json")
 *     ),
 *     @OA\Response(response="422", description="Validation error",
 *         @OA\MediaType(mediaType="application/json")
 *     ),
 *  )
 */

/**
 *  @OA\Delete(path="/product/delete/{id}", description="", summary="Delete product",
 *     tags={"Product"}, security={{ "BearerAuth"={} }},
 *     @OA\Parameter(in="path", name="id", required=true, example=1),
 *     @OA\Response(response="200", description="product deleted successfully",
 *         @OA\MediaType(mediaType="application/json")
 *     ),
 *     @OA\Response(response="401", description="Unauthenticated",
 *         @OA\MediaType(mediaType="application/json")
 *     ),
 *  )
 */


 ?>
