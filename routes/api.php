<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\ProductController;

// use App\Http\Controllers\Api\ProductController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(
    ['prefix' => 'v1'],
    function () {
        Route::post('register', [ UserController::class, 'register']);
        Route::post('login', [ UserController::class, 'login']);


        Route::group(['prefix' => 'category', 'middleware' => ['auth:api']], function () {

            Route::get('list',[CategoryController::class, 'index']);
            Route::post('create',[CategoryController::class, 'store']);
            Route::put('update/{id}',[CategoryController::class, 'update']);
            Route::delete('delete/{id}',[CategoryController::class, 'delete']);

        });
        Route::group(['prefix' => 'product', 'middleware' => ['auth:api']], function () {
            Route::get('{category_id}/list',[ProductController::class, 'categoryProduct']);
            Route::get('list',[ProductController::class, 'index']);
            Route::post('create',[ProductController::class, 'store']);
            Route::put('update/{id}',[ProductController::class, 'update']);
            Route::delete('delete/{id}',[ProductController::class, 'delete']);

        });





    }
);

